<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Entities\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});


$factory->define(App\Entities\Product::class, function (Faker\Generator $faker) {

    $images = [
        '//cdn.shopify.com/s/files/1/0098/8822/products/Vital_Seamless_Leggings_Deep_Magenta_B-Edit_HK_1024x1024.jpg?v=1551108089',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Vital_Seamless_Leggings_Malibu_Blue_Marl_A-Edit_HK_1024x1024.jpg?v=1551108031',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Slounge_Crescent_Hoodie_Dark_Ruby_Marl_A-Edit_HK_1024x1024.jpg?v=1544524053',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Flawless_Knit_Tights_Black_A_1024x1024.jpg?v=1543245026',
        '//cdn.shopify.com/s/files/1/0098/8822/products/So_Soft_-_Taupe_Marl_A-Edit_1024x1024.jpg?v=1533208594',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Cosy_Pullover_Black_Marl_A-Edit_HK_1024x1024.jpg?v=1546852794',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Elevate_Cycle_Shorts_Indigo_A-Edit_HK_1024x1024.jpg?v=1546852835',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Flawless_Knit_LS_Crop_Top_Black_A_1024x1024.jpg?v=1543244886',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Primary_Cross_Back_LS_Top_Pop_Red_A_fb7f8777-c3ab-4a36-9277-2d26841e5cce_1024x1024.jpg?v=1541494715',
        '//cdn.shopify.com/s/files/1/0098/8822/products/True_Texture_Vest_-_Black_A_1024x1024.jpg?v=1551278837',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Bold_SS_T-Shirt_Black_B-Edit_HK_450x.jpg?v=1551106501',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Critical_SS_T-Shirt_Chalk_Grey_A-Edit_DW_1024x1024.jpg?v=1551107225',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Critical_Hoodie_Black_A-Edit_ZH_1024x1024.jpg?v=1551106855',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Apollo_SS_T_Shirt_Smokey_Grey_A-Edit_ZH_1024x1024.jpg?v=1551106312',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Ultra_Jacquard_Bottoms_Woodland_Green_A_1024x1024.jpg?v=1548686813',
        '//cdn.shopify.com/s/files/1/0098/8822/products/Ark_Bottoms_Grey_A-Edit_HK_1024x1024.jpg?v=1550829551',
        '//cdn.shopify.com/s/files/1/0098/8822/products/IMG_1685_1024x1024.jpg?v=1531836828',
        '//cdn.shopify.com/s/files/1/0098/8822/products/ark_sleevless_t_shirt_black_5_of_6_1024x1024.jpg?v=1522834821'


    ];

    return [
        'uuid' => $faker->uuid,
        'name' => $faker->productName,
        'price' => $faker->randomFloat(2, 3.00, 20.00),
        'image' => $faker->randomElement($images),
        'description' => $faker->sentences(mt_rand(2, 10), true),
        'available' => $faker->boolean(80),
        'stock' => $faker->randomNumber(4),
    ];
});
