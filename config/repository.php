<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Repositories to bind
    |--------------------------------------------------------------------------
    |
    |   Example:
    |   [
    |     'concrete' => App\Repositories\UserRepository::class,
    |     'entity' => \App\Entities\User::class,
    |     'repository' => App\Repositories\Eloquent\EloquentUserRepository::class,
    |     'cache_decorator' => App\Repositories\Cache\CacheUserDecorator::class,
    |   ]
    |
    */

    'repositories' => [
        [
            'concrete' => App\Repositories\ProductRepository::class,
            'entity' => \App\Entities\Product::class,
            'repository' => App\Repositories\Eloquent\EloquentProductRepository::class,
            'cache_decorator' => App\Repositories\Cache\CacheProductDecorator::class,
        ],
        [
            'concrete' => App\Repositories\PackRepository::class,
            'entity' => \App\Entities\Pack::class,
            'repository' => App\Repositories\Eloquent\EloquentPackRepository::class,
            'cache_decorator' => App\Repositories\Cache\CachePackDecorator::class,
        ]
    ],

    'cache' => [

        /*
        |--------------------------------------------------------------------------
        | Enable caching:
        | This will use the caching method specified in config/cache.php
        |--------------------------------------------------------------------------
        */

        'enabled' => false,

        /*
        |--------------------------------------------------------------------------
        | Cache time: How long should the cache hold data
        |--------------------------------------------------------------------------
        */

        'time' => 30
    ]
];
