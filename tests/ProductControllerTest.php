<?php

use App\Services\PackCalculator;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ProductControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    public function testShouldReturnFirstPageOfProducts()
    {
        $response = $this->get('/products');
        $response->assertResponseOk();
        $response->seeJsonContains([
            'status' => "success",
            "current_page" => 1,
        ]);
    }

    public function testShouldReturnCorrectPerPage()
    {
        for ($i = 1; $i <= 50; $i++) {
            $response = $this->get("/products?perPage={$i}");
            $response->assertResponseOk();
            $responseData = json_decode($this->response->content());
            $this->assertEquals($i, count($responseData->resource->data));
        }
    }

    public function testShouldReturnInPriceAscendingOrder()
    {
        $response = $this->get("/products?orderBy=price&orderDir=asc");
        $response->assertResponseOk();
        $responseData = json_decode($this->response->content());

        $first = $responseData->resource->data[0]->price;
        $next = $responseData->resource->data[1]->price;

        $this->assertLessThanOrEqual($next, $first);
    }

    public function testShouldReturnInPriceDescendingOrder()
    {
        $response = $this->get("/products?orderBy=price&orderDir=desc");
        $response->assertResponseOk();
        $responseData = json_decode($this->response->content());

        $first = $responseData->resource->data[0]->price;
        $next = $responseData->resource->data[1]->price;

        $this->assertGreaterThanOrEqual($next, $first);
    }

    public function testShouldReturnSingleProduct()
    {
        $product = $this->getRandomSingleProduct();

        $response = $this->get("/product/{$product->uuid}");
        $response->assertResponseOk();

        $responseData = json_decode($this->response->content());

        $this->assertEquals($responseData->resource, $product);
    }

    private function getRandomSingleProduct()
    {
        $response = $this->get("/products?orderBy=price&orderDir=desc&perPage=50");
        $response->assertResponseOk();
        $responseData = json_decode($this->response->content());

        $products = $responseData->resource->data;

        return $products[array_rand($products)];
    }
}
