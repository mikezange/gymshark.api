<?php

use App\Services\PackCalculator;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PackCalculatorServiceTest extends TestCase
{
    /** @var PackCalculator */
    private $packCalculator;

    public function setUp()
    {
        parent::setUp();
        $this->packCalculator = $this->app->make(PackCalculator::class);
    }

    /**
     * Run array assertion for supplied packs and numbers
     *
     * @param array $packs
     * e.g. [250, 500, 1000]
     *
     * @param array $numbersOutput supplied number as index  with expected output as value
     * e.g. [12001 => [5000 => 2, 2000 => 1, 250 => 1]]
     */
    private function runTestForPacksAndNumbers(array $packs, array $numbersOutput)
    {
        $this->packCalculator->setAvailablePackSizes($packs);

        foreach ($numbersOutput as $number => $expectedOutput) {
            $result = $this->packCalculator->getPacks($number);
            $this->assertEquals($expectedOutput, $result);
        }
    }

    /**
     * Tests for supplied cases
     *
     * @return void
     */
    public function testSuppliedCases()
    {
        $packs = [250, 500, 1000, 2000, 5000];

        $numbersOutput = [
            1 => [250 => 1],
            250 => [250 => 1],
            251 => [500 => 1],
            501 => [500 => 1, 250 => 1],
            12001 => [5000 => 2, 2000 => 1, 250 => 1]
        ];

        $this->runTestForPacksAndNumbers($packs, $numbersOutput);
    }

    /**
     * Tests for additional cases with supplied packs
     *
     * @return void
     */
    public function testAdditionalCasesWithSuppliedPacks()
    {
        $packs = [250, 500, 1000, 2000, 5000];

        $numbersOutput = [
            750 => [500 => 1, 250 => 1],
            751 => [1000 => 1],
            1050 => [1000 => 1, 250 => 1],
            10000 => [5000 => 2],
            20261 => [5000 => 4, 500 => 1],
        ];

        $this->runTestForPacksAndNumbers($packs, $numbersOutput);
    }

    /**
     * Test for packs in multiples of 300
     *
     * @return void
     */
    public function testMultiplesOf300()
    {
        $packs = [300, 600, 1200, 2400, 4800];

        $numbersOutput = [
            300 => [300 => 1],
            600 => [600 => 1],
            900 => [600 => 1, 300 => 1],
            1500 => [1200 => 1, 300 => 1],
            10000 => [4800 => 2, 600 => 1],
        ];

        $this->runTestForPacksAndNumbers($packs, $numbersOutput);
    }


    /**
     * Test odd pack sizes
     *
     * @return void
     */
    public function testOddPackSizes()
    {
        $packs = [250, 501, 1000, 2000, 5000];

        $numbersOutput = [
            501 => [501 => 1],
            1002 => [501 => 2],
            1003 => [1000 => 1, 250 => 1],
        ];

        $this->runTestForPacksAndNumbers($packs, $numbersOutput);
    }

    /**
     * Test for checking packs do not overlap incorrectly
     *
     * @return void
     */
    public function testOverlapWithSuppliedPacks()
    {
        $packs = [250, 500, 1000, 2000, 5000];

        $numbersOutput = [
            249 => [250 => 1],
            251 => [500 => 1],
            499 => [500 => 1],
            999 => [1000 => 1],
            1001 => [1000 => 1, 250 => 1],
            1999 => [2000 => 1],
            2001 => [2000 => 1, 250 => 1],
            4999 => [5000 => 1],
            5001 => [5000 => 1, 250 => 1]
        ];

        $this->runTestForPacksAndNumbers($packs, $numbersOutput);
    }
}
