<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('/products', ['uses' => 'ProductController@index']);
$router->get('/product/{uuid}', ['uses' => 'ProductController@single']);
$router->put('/product/{uuid}', ['uses' => 'ProductController@modifyPack']);
$router->post('/product-packs', ['uses' => 'ProductController@calculatePacks']);

$router->get('/packs', ['uses' => 'PackController@index']);
$router->post('/pack', ['uses' => 'PackController@create']);
$router->put('/pack', ['uses' => 'PackController@modify']);
$router->delete('/pack', ['uses' => 'PackController@delete']);
