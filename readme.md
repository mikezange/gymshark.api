## Mike Zange

API back end for requested technical test, built with Lumen.

## Setup

Clone this repository.

Run `composer install` from the root folder.

Then you can either:

- Use PHP's built in server ` php -S localhost:8000 -t public`
- Use Laravel Homestead
- Use a domain and use `public` as web root


## Testing

To run the unit tests please run: ` ./vendor/bin/phpunit` from the root folder. 