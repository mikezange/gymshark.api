<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'name', 'price', 'image', 'description', 'available', 'stock'
    ];

    protected $with = [
        'availablePacks'
    ];

    protected $casts = [
        'available' => "bool"
    ];

    public function availablePacks()
    {
        return $this->belongsToMany(Pack::class, 'product_pack_pivot');
    }
}
