<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'pack_size',
    ];

    protected $casts = [
      'enabled' => 'boolean'
    ];

    protected $hidden = [
        'pivot'
    ];
}
