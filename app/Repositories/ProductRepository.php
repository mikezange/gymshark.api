<?php

namespace App\Repositories;

use MikeZange\LaravelEntityRepositories\Repositories\BaseRepository;

interface ProductRepository extends BaseRepository
{
}