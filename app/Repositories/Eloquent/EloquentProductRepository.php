<?php

namespace App\Repositories\Eloquent;

use App\Repositories\ProductRepository;
use MikeZange\LaravelEntityRepositories\Repositories\Eloquent\EloquentBaseRepository;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
}
