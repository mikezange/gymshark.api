<?php

namespace App\Repositories\Eloquent;

use App\Repositories\PackRepository;
use MikeZange\LaravelEntityRepositories\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPackRepository extends EloquentBaseRepository implements PackRepository
{
}
