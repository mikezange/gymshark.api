<?php

namespace App\Repositories\Cache;

use App\Repositories\PackRepository;
use MikeZange\LaravelEntityRepositories\Repositories\Cache\CacheBaseDecorator;

class CachePackDecorator extends CacheBaseDecorator implements PackRepository
{
    public function __construct(PackRepository $repository)
    {
        parent::__construct();
        $this->entityName = 'pack';
        $this->repository = $repository;
    }
}