<?php

namespace App\Repositories\Cache;

use App\Repositories\ProductRepository;
use MikeZange\LaravelEntityRepositories\Repositories\Cache\CacheBaseDecorator;

class CacheProductDecorator extends CacheBaseDecorator implements ProductRepository
{
    public function __construct(ProductRepository $repository)
    {
        parent::__construct();
        $this->entityName = 'product';
        $this->repository = $repository;
    }
}