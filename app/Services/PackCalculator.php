<?php

namespace App\Services;

class PackCalculator
{
    private $availablePackSizes;
    private $smallestPackSize;
    private $packs;

    /**
     * @param array $availablePackSizes
     */
    public function setAvailablePackSizes(array $availablePackSizes)
    {
        //sort the packages in decrementing order
        rsort($availablePackSizes);
        $this->availablePackSizes = $availablePackSizes;
        $this->smallestPackSize = end($this->availablePackSizes);
    }

    /**
     * @param null $items
     * @return array
     */
    public function getPacks($items = null)
    {
        $this->packs = $this->consolidateToPacks($items);
        return array_count_values($this->packs);
    }

    public function getCount()
    {
        return array_sum($this->packs);
    }

    /**
     * Consolidate the minimum items to packs
     *
     * @param $items
     * @param bool $consolidate
     * @return array
     */
    private function consolidateToPacks($items, $consolidate = true)
    {
        $packs = [];
        
        while ($items > 0) {
            $pack = $this->getPackSize($items);
            $packs[] = $pack;
            $items -= $pack;
        }

        if ($consolidate) {
            $packs = $this->consolidateToPacks(array_sum($packs), false);
        }

        return $packs;
    }

    /**
     * Returns the idea pack size for the given items
     *
     * @param $items
     * @return int|null
     */
    private function getPackSize($items)
    {
        if ($items <= $this->smallestPackSize) {
            return $this->smallestPackSize;
        }

        foreach ($this->availablePackSizes as $index => $packSize) {
            if ($items % $packSize === 0) {
                return $packSize;
            }
        }

        foreach ($this->availablePackSizes as $index => $packSize) {
            if ($items >= $packSize) {
                return $packSize;
            }
        }

        return null;
    }
}
