<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use App\Services\PackCalculator;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @param ProductRepository $productRepository
     */
    public function __construct(Request $request, ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->request = $request;
    }

    /**
     * All Products (Listing Page)
     * @return mixed
     */
    public function index()
    {
        $all = $this->request->query('all', false);

        if ($all) {
            $data = $this->productRepository->all();
        } else {
            $perPage = $this->request->query('perPage', 15);
            $orderBy = $this->request->query('orderBy', 'id');
            $orderDir = $this->request->query('orderDir', 'asc');

            $data = $this->productRepository->paginate($perPage, $orderBy, $orderDir);
        }

        if ($data->count() <= 0) {
            return response()->json([
                'status' => "error",
                'message' => "No records retrieved",
                'resource' => []
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Products Found',
            'resource' => $data
        ], 200);
    }

    /**
     * Get a single product
     *
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function single($uuid)
    {
        $product = $this->productRepository->firstByAttributes(['uuid' => $uuid]);

        if (!$product) {
            return response()->json([
                'status' => "error",
                'message' => "Product not found",
                'resource' => []
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Product Found',
            'resource' => $product
        ], 200);
    }

    /**
     * Calculate packs for number of items
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculatePacks()
    {
        $uuid = $this->request->input('product_uuid');
        $number = $this->request->input('number');

        $product = $this->productRepository->firstByAttributes(['uuid' => $uuid]);

        if (!$product) {
            return response()->json([
                'status' => "error",
                'message' => "Product not found",
                'resource' => []
            ], 404);
        }

        if (count($product->availablePacks) <= 0) {
            return response()->json([
                'status' => "error",
                'message' => "No packs available",
                'resource' => []
            ], 422);
        }

        /** @var $packCalculatorService PackCalculator */
        $packCalculatorService = app()->make(PackCalculator::class);

        $packCalculatorService->setAvailablePackSizes($product->availablePacks->pluck('pack_size')->toArray());

        $packs = $packCalculatorService->getPacks($number);
        $totalPrice = $product->price * $packCalculatorService->getCount();

        return response()->json([
            'status' => 'success',
            'message' => 'Packs Found',
            'resource' => [
                'packs' => $packs,
                'price' => number_format($totalPrice, 2)
            ]
        ], 200);
    }

    /**
     * Modify a pack attached to a product
     *
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function modifyPack($uuid)
    {
        $product = $this->productRepository->firstByAttributes(['uuid' => $uuid]);

        if (!$product) {
            return response()->json([
                'status' => "error",
                'message' => "Product not found",
                'resource' => []
            ], 404);
        }

        $packId = $this->request->input('pack_id');
        $method = $this->request->input('method');

        if ($method === "attach") {
            $product->availablePacks()->attach($packId);
        } elseif ($method === "detach") {
            $product->availablePacks()->detach($packId);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Product updated',
            'resource' => $product
        ], 200);
    }
}
