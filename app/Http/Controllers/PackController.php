<?php

namespace App\Http\Controllers;

use App\Repositories\PackRepository;
use App\Repositories\ProductRepository;
use App\Services\PackCalculator;
use Illuminate\Http\Request;

class PackController extends Controller
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var PackRepository
     */
    private $packRepository;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @param PackRepository $packRepository
     */
    public function __construct(Request $request, PackRepository $packRepository)
    {
        $this->packRepository = $packRepository;
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $data = $this->packRepository->all();

        if ($data->count() <= 0) {
            return response()->json([
                'status' => "error",
                'message' => "No records retrieved",
                'resource' => []
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Packs Found',
            'resource' => $data
        ], 200);
    }

    public function create()
    {
        $data = [
            'name' => $this->request->input('name'),
            'pack_size' => $this->request->input('pack_size'),
        ];

        $pack = $this->packRepository->create($data);

        return response()->json([
            'status' => 'success',
            'message' => 'Pack created',
            'resource' => $pack
        ], 200);
    }

    public function delete()
    {
        $id = $this->request->input('packId');

        $deleted = $this->packRepository->deleteById($id);

        if ($deleted) {
            return response()->json([
                'status' => 'success',
                'message' => 'Pack deleted',
                'resource' => []
            ], 200);
        }
    }

    public function modify()
    {
        $input = $this->request->input();

        $pack = $this->packRepository->firstByAttributes(['id' => $input['id']]);

        if (!$pack) {
            return response()->json([
                'status' => 'error',
                'message' => 'Pack not found',
                'resource' => []
            ], 404);
        }

        $this->packRepository->update($pack, $input);

        return response()->json([
            'status' => 'success',
            'message' => 'Pack updated',
            'resource' => $pack
        ], 200);
    }
}
